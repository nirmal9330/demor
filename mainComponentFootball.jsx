import React,{Component} from "react";
import NewMatch from "./newMatchFootball";
import NavbarFootball from "./navbarFootball";
import ScorePage from "./scorePageFootball";
import AllMatch from "./allMatchFootball"
import PointsTableFootball from "./pointsTableFootball";


class FootballMain extends Component{
   state={
       teams:["France","England","Brazil","Germany", "Argentina"],
       pointsTable:[],
       allMatch :[],
       matches :{
        team1:"",
        team2:"",
        point1:0,
        point2:0,
        oPoint:null,
        result:"",
},
       
       
       shownView:-1,
    };
    createPointTable=(teams)=>
    {
      let  PointsTables=[];
       teams.map((tm)=>
       {
    let PointsTable={team : tm ,played:0,won:0,lost:0,drawn:0,goalFor:0,goalAgainest:0,points:0};
    PointsTables.push(PointsTable);
    })
    return PointsTables;
    }
    viewHandler=(index)=>
    {
        let s1={...this.state};
      
        s1.shownView=index;
        this.setState(s1);
    }
    onAddTeamHandler=(team1,team2)=>
    {
        let s1={...this.state};
        s1.matches.team1=team1;
        s1.matches.team2=team2;
       s1.shownView=4;
        this.setState(s1);
    }
    MatchOverHandler=(json)=>
    {
  let s1={...this.state};
  if(s1.matches.point1===s1.matches.point2) 
  {
      s1.matches.result="Match drawn";
  }
  s1.allMatch.push(json);
  s1.shownView=-1;
  s1.matches ={
    team1:"",
    team2:"",
    point1:0,
    point2:0,
    oPoint:null,
    result:"",};
    
  this.setState(s1);
    }
    showAllMatchTable=()=>
    {
        let s1={...this.state};
       
        return(<React.Fragment>
             {this.showButton()};
            <div className="container">
            <div className="row  bg-dark text-center text-white border">
                    <div className="col-3">Team1</div>
                    <div className="col-3">Team2</div>
                    <div className="col-3">Score</div>
                    <div className="col-3">Result</div>
                </div>
            {s1.allMatch.map((mtch)=>(
                <div className="row border text-center">
                    <div className="col-3">{mtch.team1}</div>
                    <div className="col-3">{mtch.team2}</div>
                    <div className="col-3">{mtch.point1+'-'+mtch.point2}</div>
                    <div className="col-3">{mtch.result}</div>
                </div>
            ))}
            </div>
        </React.Fragment>);
    }
    showButton=()=>
    {
        return (<React.Fragment>
            <button className="btn btn-primary m-2"onClick={()=>this.viewHandler(1)}>All Match</button>
            <button className="btn btn-primary m-2"onClick={()=>this.viewHandler(2)}>Points Table</button>
            <button className="btn btn-primary m-2"onClick={()=>this.viewHandler(3)}>Add Match</button>
            </React.Fragment>);
    }
render()
{
   
    let {teams,shownView,matches,allMatch} =this.state;
    let PointTables=this.createPointTable(teams);
   
    return(<React.Fragment>
      <NavbarFootball  allMatch={allMatch} />
      {shownView<=0 ?
         this.showButton()
     : ""}
      {shownView===1 ?<React.Fragment>{this.showButton()};<AllMatch allMatch={allMatch}/></React.Fragment>: ""}
      {shownView===2 ?<React.Fragment>   {this.showButton()};
          <PointsTableFootball PointTables={PointTables} allMatch={allMatch}/></React.Fragment>: ""}
       {shownView===3 ?  <React.Fragment>{this.showButton()};   <NewMatch teams={teams} onAdd={this.onAddTeamHandler}/>
     </React.Fragment> :
       
       
       
       ""}
       {shownView===4 ? 
       
       <React.Fragment><ScorePage  matches={matches} onMatchOver={this.MatchOverHandler}/></React.Fragment> : ""}

     
    </React.Fragment>);
}
   }
   export default FootballMain;
