import React,{Component} from "react";
class ProductDetails extends Component
{
 state={
    products:[{"product":"Pepsi", "sales":[2,5,8,10,5]},
    {"product":"Coke", "sales":[3,6,5,4,11,5]},
    {"product":"5Star", "sales":[10,14,22]},
    {"product":"Maggi", "sales":[3,3,3,3,3]},
    {"product":"Perk", "sales":[1,2,1,2,1,2]},
    {"product":"Bingo", "sales":[0,1,0,3,2,6]},
    {"product":"Gems", "sales":[3,3,1,1]},],
    viewIndex:-1,
    index:-1,
 };
 showProductDetailsHandler=(index)=>
 {
     let s1={...this.state};
     s1.viewIndex=index;
     this.setState(s1);
 }
 showProductDetails=(index)=>
 {
        let s1={...this.state};
        let product=s1.products[index];
        return(<React.Fragment>
            <ul>
                <li>Product :{product.product}</li>
        {product.sales.map((sal,index)=>
        (
           <li>Store {index+1} : {sal}</li> 
        ))}
                </ul>
        </React.Fragment>);
 }
 sortProduct=(index)=>
 {
  let s1={...this.state};
   let prArr= index===1 ? s1.products.sort((n1,n2)=>n1.product.localeCompare(n2.product)) :
      index===2 ? s1.products.sort((n1,n2)=>n1.sales.reduce((acc,cur)=>acc+cur,0)-
      n2.sales.reduce((acc,cur)=>acc+cur,0))  : index===3 ?  s1.products.sort((n1,n2)=>n2.sales.reduce((acc,cur)=>acc+cur,0)-
      n1.sales.reduce((acc,cur)=>acc+cur,0)) : s1.products;
   return prArr;
   
 }
   
 sortProductHandler=(ind)=>
 {
     let s1={...this.state};
     s1.viewIndex=-1;
     s1.index=ind;
     this.setState(s1);
 }
 

    render()
    {
     
    let  {products,viewIndex,index} =this.state;
    let arr = index>0 ? this.sortProduct(index) : products;
       return (<React.Fragment>
            <div className="container">
               <h3>Welcome To Product Portal</h3>
   <button className="btn btn-primary m-3" onClick={()=>this.sortProductHandler(1)}>Sort By Product</button>
    <button className="btn btn-primary m-3" onClick={()=>this.sortProductHandler(2)}>Total Sales Asc</button>
     <button className="btn btn-primary m-3" onClick={()=>this.sortProductHandler(3)}>Total Sales Desc</button>
        <div className="row">
            <div className="col-9">
          <div className="row border text-center bg-dark text-white">
              <div className="col-4 border">Product</div>
              <div className="col-4 border">Total Sales</div>
              <div className="col-4 border"></div>
              </div>     
         {arr.map((pr,index)=>(
           <div className="row border text-center text-bold">
           <div className="col-4 border">{pr.product}</div>
           <div className="col-4 border">{pr.sales.reduce((acc,cur)=>acc+cur,0)}</div>
           <div className="col-4 border"><button className="btn btn-primary"
           onClick={()=>this.showProductDetailsHandler(index)}>Details</button></div>
           </div> 
         ))}
         </div>
         <div className="col-3 bg-light border">
           {viewIndex>=0 ? this.showProductDetails(viewIndex):""}
           </div>
        </div>
        </div>
       </React.Fragment>);
    }
}
export default ProductDetails;