import React,{Component} from "react";
class AllMatch extends Component
{



    render()
    {
        let {allMatch} =this.props;
      return(
          <React.Fragment>
              {allMatch.length!=0 ?
             <div className="container">
             <div className="row  bg-dark text-center text-white border">
                     <div className="col-3">Team1</div>
                     <div className="col-3">Team2</div>
                     <div className="col-3">Score</div>
                     <div className="col-3">Result</div>
                 </div>
             {allMatch.map((mtch)=>(  <React.Fragment>
                 <div className="row border text-center">
                     <div className="col-3">{mtch.team1}</div>
                     <div className="col-3">{mtch.team2}</div>
                     <div className="col-3">{mtch.point1+'-'+mtch.point2}</div>
                     <div className="col-3">{mtch.result}</div>
                 </div>
                 </React.Fragment>
             ))}
             </div>
              : <h3 className="text-center">No Teams To Show</h3> }
          
         </React.Fragment>);
    }
}
export default AllMatch;