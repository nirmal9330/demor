import React,{Component} from "react";
import {getQuizQuestions} from "./quizqns";

class QuizGame extends Component
{
state ={
    players : [
    {name:"James",points:0},
    {name:"Julia",points:0},
    {name:"Maratha",points:0},
    {name:"Smith",points:0},],

    questions: getQuizQuestions(),
    currentQuestion :0,
    playerBuzzer : -1,
    buzzerStatus: -1,
    showAnswer:false,
   
}
handleOption=(index,answer)=>
{
    let s1={...this.state};
    
    if(s1.currentQuestion===s1.questions.length-1)
    {
        s1.showAnswer=true;
    }
    if(s1.playerBuzzer===-1||s1.buzzerStatus===-1)
    {
        alert("press Buzzer status");
    }
    else{
    if(s1.currentQuestion!=s1.questions.length)
    {
    if(index===answer)
    {
      
        alert("Correct Answer You Got +3 Points");
       s1.players[s1.playerBuzzer].points+=3;
       s1.currentQuestion++; 
       s1.playerBuzzer=-1;
    }
    else if(index!=answer)
    {
      alert("Wrong Answer You got -1 points");
      s1.players[s1.playerBuzzer].points-=1;
      s1.currentQuestion++; 
      s1.playerBuzzer=-1;
    }
}
    }

s1.buzzerStatus=-1;
s1.playerBuzzer=-1;
this.setState(s1);
}
showAnswerStatus=()=>
{
    let s1={...this.state};
    let maxScore= s1.players.reduce((acc,cur)=>{
        return acc>cur.points ? acc : cur.points;
    },0);
    let player = s1.players.reduce((acc,cur)=>{
       if(cur.points===maxScore){
           acc.push(cur.name) 
    }
    return acc;
},[]);
  
    return (<React.Fragment>
        <h3 className="text-center">Game Over</h3>
        <h4 className="text-center">Winner Is :{player.map((p)=>
            p + ","
        )} </h4>
        
       
    </React.Fragment>);
}
handleBuzzer=(index)=>
{
   let s1={...this.state};
 
   s1.playerBuzzer=index;
      s1.buzzerStatus=index;
      this.setState(s1);
}
getDisplayClass=(index)=>
{
  let s1={...this.state};
   let color=  s1.buzzerStatus===index ? " bg-success " : " bg-warning ";
   return color;
}
render()
    {
        let {players,questions,currentQuestion,playerBuzzer,showAnswer}=this.state;
        console.log(showAnswer);
       return (<React.Fragment>
           <div className="container">
               <div className="row">
                   <div className="col-4 mx-auto">
               <h2>Welcome To Quiz Contest</h2>
               <h3 className="text-center">Participants</h3>
               </div>
             
               <div className="col-12 text-center ">
               <div className="row">
              
             { players.map((pl,index)=>
             (
                  <div className= {" col-3 border text-center m-4 " + this.getDisplayClass(index)} 
                  style={{width:'20%'} } >
                     <h4>Name :{pl.name}</h4>
                     <h5>Score :{pl.points}</h5>
                     <button className="btn bg-white" 
                     onClick={()=>this.handleBuzzer(index)}>BUZZER</button>
                  </div> 
             )) }
           
             </div>
             </div>
         
             <div className="row mt-2">
               <div className="col-4 mx-auto text-center">
              {!showAnswer ?<React.Fragment>
                  <h4 className="text-center">Question Number : {currentQuestion+1}</h4>
                  <h4 className="text-center">{questions[currentQuestion].text}</h4>
                  {questions[currentQuestion].options.map((op,index)=>(
                     <button className="btn bg-info m-2" onClick={()=>this.handleOption((index+1),
                     questions[currentQuestion].answer)}>
                         {op}</button>
                  ))}
              </React.Fragment> :""}
                   </div>
               </div></div>
               <div className="col-6 mx-auto">
                   { showAnswer? this.showAnswerStatus() : "" }
               </div>
               </div>
                     
       </React.Fragment>)
    }
}
export default QuizGame;