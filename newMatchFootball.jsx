import React,{Component} from "react";
class NewMatch extends Component{
   state={
       team1:"",
       team2:"",
   };
    AddMatch=(str,str1)=>
    {
        let {teams}=this.props;
        let s1={...this.state}
        if(str==="1") 
s1.team1=str1;
else if(str==="2" )
s1.team2=str1 ;
this.setState(s1);
    }
    StartHandler=()=>
   {
    let s1={...this.state};
    s1.team1==="" && s1.team2==="" ? alert("Select Team") :
    s1.team1=== s1.team2 ? alert("Team Must be Different") ?
     s1.team1==="" ? alert("select Team 1") : s1.team2==="" ? alert("Select Team2") :<React.Fragment>""</React.Fragment>
     :<React.Fragment>""</React.Fragment>:<React.Fragment>""</React.Fragment>;
    if(s1.team1!=s1.team2)
    this.props.onAdd(s1.team1,s1.team2);
   }
    render()
    {
        let {team1,team2}=this.state;
        let  {teams}=this.props;
       return(<React.Fragment>
         
           <div className="container text-center">
               <div className="col-12 text-center mx-auto">
              <h3> Choose Team1 :{team1}</h3>

               </div>
               <div className="col-12 d-flex justify-content-around">
                   {teams.map((tm,index)=>(
              <button className="btn btn-warning text-center text-white " onClick={()=>this.AddMatch("1",tm)}>{tm}</button>
                   ))}
               </div><br/><br/>
               <div className="col-12 text-center mx-auto">
                 <h3> Choose Team2 :{team2}</h3>

               </div>
               <div className="col-12 d-flex justify-content-around">
                   {teams.map((tm)=>(
                       <button className="btn btn-warning text-center text-white" 
                       onClick={()=>this.AddMatch("2",tm)}>{tm}</button>
                   ))}
               </div><br/>
               <button className="btn btn-dark" onClick={()=>this.StartHandler()}>Start Match</button>
               </div>
       </React.Fragment>);
    }
}
export default NewMatch;