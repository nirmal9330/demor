import React,{Component} from "react";
class ScorePage extends Component
{
state={
      matches:this.props.matches,
};
handleGoal=(str)=>
{
    let s1={...this.state};
  
    if(str==="team1")
    s1.matches.point1++;
  
    if(str==="team2")
  s1.matches.point2++;
 
  if( s1.matches.point1> s1.matches.point2)
  {
      s1.matches.result=s1.matches.team1+" Won";
  }
  else  if( s1.matches.point2> s1.matches.point1)
  {
      s1.matches.result=s1.matches.team2+" Won";
  }
  else  if(s1.matches.point2===s1.matches.point1)
  {
      s1.matches.result="Match Drawn";
  }
  else 
  {
      s1.matches.result="Match Drawn";
  }
  
 
this.setState(s1);
}
matchOverHandler=()=>
{
   this.props.onMatchOver(this.state.matches);
}
render()
{
  
    let {matches}=this.props;

    let {team1,team2,point1,point2,oPoint}=this.state.matches;
    console.log(team1,point1);
  
   return(<React.Fragment>
         <div className="container">
           
                 <h3 className="display-4 text-center">Welcome To Exiciting Match</h3>
                 <div className="col-12 text-center d-flex justify-content-around">
                     
                     <h2>{team1}</h2>
                     <h3>{point1+'-'+point2}</h3>
                     <h2>{team2}</h2>
                     </div>
                     <div className="row">
                         <div className="col-1"></div>
                   <div className="col-2  text-center">
                 <button className="btn btn-warning" style={{marginLeft:'1rem'}} onClick={()=>this.handleGoal("team1")}>Goal Scored</button>
                 </div>
                 <div className="col-5"></div>
                 <div className="col-3 tex-center">
                 <button className="btn btn-warning" style={{marginLeft:'8rem'}} onClick={()=>this.handleGoal("team2")}>Goal Scored</button>
                </div>
                </div>
     </div>
             <div className="text-center"><button className="btn btn-warning" onClick={()=>this.matchOverHandler()}>Match Over</button></div>       
</React.Fragment>)
}
}
export default ScorePage;